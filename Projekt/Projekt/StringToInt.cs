﻿using System;

namespace Projekt
{
    public static class StringToInt
    {
        public static int[] ZamienNaInt(this string[] dataString)
        {
            var data = new int[dataString.Length];
            for (int i = 0; i < dataString.Length; i++)
            {
                data[i] = Convert.ToInt32(dataString[i]);
            }
            return data;
        }
    }
}