﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Projekt
{
    public partial class MainWindow : Window
    {
        // ZMIENNE ---------------
        private const int RozmiarCzworokata = 4;

        private const int RozmiarBitmapy = 32;
        private int[][] _wczytaneDane;
        public int[][] MapaBitmap { get; set; }
        // -----------------------

        public MainWindow()
        {
            InitializeComponent();
        }

        private void WczytajBibliotekeDanych()
        {
            try
            {
                var plikOdczytany = File.ReadAllLines("digits.txt");
                _wczytaneDane = new int[plikOdczytany.Length][];
                for (var i = 0; i < plikOdczytany.Length; i++)
                {
                    _wczytaneDane[i] = plikOdczytany[i].Split(',').ZamienNaInt();
                }
            }
            catch (Exception e)
            {
                ErrorTB.Text = e.ToString();
            }
            finally
            {
                ErrorTB.Text = "Plik wczytany pomyślnie";
            }
        }

        private void MapaBitmapUtworzTablice()
        {
            MapaBitmap = new int[RozmiarBitmapy][];
            for (var i = 0; i < RozmiarBitmapy; i++)
            {
                MapaBitmap[i] = new int[RozmiarBitmapy];
            }
        }

        
        private List<Point> ListaPunktowPlotna()
        {
            List<Point> punktyPlotnaSrodek = new List<Point>();
            foreach (var stroke in Drawer.Strokes)
            {
                Geometry sketchGeo = stroke.GetGeometry();
                Rect strokeBounds = sketchGeo.Bounds;

                for (int x = (int)strokeBounds.TopLeft.X; x < (int)strokeBounds.TopRight.X + 1; x++)
                {
                    for (int y = (int)strokeBounds.TopLeft.Y; y < (int)strokeBounds.BottomLeft.Y + 1; y++)
                    {
                        Point p = new Point(x, y);

                        if (sketchGeo.FillContains(p))
                            punktyPlotnaSrodek.Add(p);
                    }
                }
            }
            return punktyPlotnaSrodek;
        }

        //--------------------------------------------
        private void WypelnijMapaBitmapy(List<Point> listaPunktow)
        {
            for (int i = 0; i < RozmiarBitmapy; i++)
            {
                for (int j = 0; j < RozmiarBitmapy; j++)
                {
                    MapaBitmap[i][j] = 0;
                }
            }
            foreach (var pktPoint in listaPunktow)
            {
                if (pktPoint.X >= 0 && pktPoint.Y >= 0 && pktPoint.X < RozmiarBitmapy && pktPoint.Y < RozmiarBitmapy)
                {
                    MapaBitmap[(int)pktPoint.X][(int)pktPoint.Y] = 1;
                }
            }
        }

        private List<int> UstawDaneLicz()
        {
            var _wczytaneDane = new List<int>();
            for (var x = 0; x < RozmiarBitmapy; x += RozmiarCzworokata)
            {
                for (var y = 0; y < RozmiarBitmapy; y += RozmiarCzworokata)
                {
                    var sumaPixeli = 0;
                    for (var i = 0; i < RozmiarCzworokata; i++)
                    {
                        for (var j = 0; j < RozmiarCzworokata; j++)
                        {
                            if (MapaBitmap[y + i][x + j] == 1)
                            {
                                sumaPixeli++;
                            }
                        }
                    }
                    _wczytaneDane.Add(sumaPixeli);
                }
            }
            return _wczytaneDane;
        }

        private int[] ZamienCanvasNaTablice()
        {
            MapaBitmapUtworzTablice();
            var listaPunktow = ListaPunktowPlotna();
            WypelnijMapaBitmapy(listaPunktow);
            var data = UstawDaneLicz();
            return data.ToArray();
        }

        private void Drawer_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var sample = ZamienCanvasNaTablice();
                var KNN = new KNN(_wczytaneDane, 10);
                WynikTB.Text = Math.Round(KNN.Szacuj(sample)).ToString();
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                Drawer.Strokes.Clear(); // usuwa zaraz po rozpoznaniu, nie widać wyrównania. Aby je zobaczyć, usuń Drawer.Strokes.Clear z tego if i odkomentuj Clear poniżej
                WynikTB.Text = string.Empty;
            }
            //Drawer.Strokes.Clear();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            WczytajBibliotekeDanych();
        }

        private void ClearCanvas_OnClick(object sender, RoutedEventArgs e)
        {
            Drawer.Strokes.Clear();
            WynikTB.Text = string.Empty;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string MessageText = "Program wykorzystuje sieć neuronową do rozpoznawania narysowanej cyfry." +
                                "\nAlgorytm sieci kNN." +
                                "\nAutor: Jakub Zahradnik";
            MessageBox.Show(MessageText);
        }
    }
}