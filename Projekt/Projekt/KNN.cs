﻿using System;
using System.Linq;

public class KNN
{
    // STRUKTURA ------------------------
    public struct StrukturaDanych
    {
        public int[] WczytaneDane { get; set; }
        public double OdlegloscPkt { get; set; }
    }

    // ----------------------------------

    // ZMIENNE --------------------------
    public StrukturaDanych[] ZbiorDanych;

    public int K { get; set; }

    // ----------------------------------

    public KNN(int[][] zbiorDanychF, int k)
    {
        ZbiorDanych = new StrukturaDanych[zbiorDanychF.GetLength(0)];
        for (int i = 0; i < zbiorDanychF.GetLength(0); i++)
        {
            ZbiorDanych[i] = new StrukturaDanych { WczytaneDane = zbiorDanychF[i] };
        }
        K = k;
    }

    private void ObliczDystansPkt(int[] sample)
    {
        for (var i = 0; i < ZbiorDanych.Length; i++)
        {
            var suma = 0;
            var dane = ZbiorDanych[i].WczytaneDane;
            for (var j = 0; j < sample.Length - 1; j++)
            {
                var roznica = dane[j] - sample[j];
                suma += roznica * roznica;
            }
            ZbiorDanych[i].OdlegloscPkt = Math.Sqrt(suma);
        }
    }

    private StrukturaDanych[] SortujZbiorDanych()
    {
        return ZbiorDanych.OrderBy(x => x.OdlegloscPkt).ToArray();
    }

    public double Szacuj(int[] sample)
    {
        ObliczDystansPkt(sample);
        ZbiorDanych = SortujZbiorDanych();

        var KNN = Nastepny(K);

        return KNN.GroupBy(x => x.WczytaneDane.Last()).OrderByDescending(x => x.Count()).FirstOrDefault().Key;
    }

    private StrukturaDanych[] Nastepny(int k)
    {
        return ZbiorDanych.Take(k).ToArray();
    }
}